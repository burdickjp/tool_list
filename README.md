# tool_list

nomenclature, standardization, and database for tooling

| parameter     | description |
|:--------------|:------------|           
| Comment       | any text in square brackets other than a part number [1/4] [#l] [#19] |
| Diameter      | dia:2.6 diameter:2.6 |
| Length of Cut | loc:0.875 LOC:0.75 |
| Radius        | R:.02 radius:0.02 |
| Flutes        | 2FL 2FLUTE (default: 2flute) |
| Type          | drill centerdrill tap ball chamfer spot flat taper bullnose lollypop flycut shearhog drag saw indexable threadmill engraver |
| Material      | carbide arb HSS CoHSS CRB diamond DMND (default: HSS) |
| Coating       | uncoated TiN AlTiN TiAlN CNB ZrN TiB2 TiB TiCN DLC nACo (default: uncoated) |
